# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 11:40:52 2019

@author: Harry Kettle
"""

#1E
from floodsystem.geo_hjk import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    #build empty list of stations
    
    N=9
    #number of stations to show
    
    rivers = rivers_by_station_number(stations, N)
    #find N rivers with most stations
    
    print('{} rivers with the most stations'.formal(N))
    print(rivers)
    
    