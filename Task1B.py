# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 17:35:56 2019

@author: User
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance


Stations = build_station_list()
    
"""Requirements for Task 1B"""

list1 = stations_by_distance(Stations, (52.2053, 0.1218))

"""Constructing new list with values in the format (name, town, distance)"""
list2 = []
for x in list1:
    station_name = x[0]
    station_distance = x[1]
    for s in Stations:
        if s.name == station_name:
            station_town = s.town
    list2.append((station_name, station_town, station_distance))

print (list2[:10])
print (list2[-10:])

