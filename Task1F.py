# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 02:28:10 2019

@author: User
"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

Stations = build_station_list()

print (inconsistent_typical_range_stations(Stations))