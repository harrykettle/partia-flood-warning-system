# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""


#1C
#from utils import sorted_by_key  # noqa

import math
import numpy as np
from haversine import haversine

#haversine function, calculates distance between coords

#stations_within_radius function parameters
centre=(52.2053, 0.1218)
r=10

#makes list of stations within radius
def stations_within_radius(stations, centre, r):
    #create empty list to hold stations within r
    stations_in_radius = []
    
    for i in stations:
        dist = haversine (centre, i.coord)
        #calculate distance from centre
        if (dist < r):
            stations_in_radius.append(i)
        #if within radius add to list
    return stations_within_radius







#1E
def rivers_by_station_number(stations, N):
    rivers = []
    #creates list of empty rivers
    for j in stations:
        if(len(rivers) == 0):
            new_river = (j.river, '1')
            rivers.append(new_river)
            #if list of rivers empty add new river+station
        else:
            river_found = False
            for l in range(len(rivers)):
                if(j.river == rivers[l][0]):
                    new_value = int(rivers[l][1])+1
                    tuple_to_change = list(rivers[l])
                    tuple_to_change[1] = str(new_value)
                    rivers[l] = tuple(tuple_to_change)
                    river_found = True
                    break;
                #if station is assigned to a river, update tuple to add station number
                #if river found break loop
            if(river_found == False):
                new_river = (j.river, '1')
                rivers.append(new_river)
                #adds river to list if not on it
                
    rivers.sort(key = lambda x: int(x[1]), reverse = True)
    #sort rivers into descending order of stations
    
    first_N_rivers = rivers[:N]
    #finds first N rivers in sorted list
    
    next_river_equal = True
    count = N
    while(next_river_equal):
        if (rivers[count][1] == first_N_rivers[count-1][1]):
            first_N_rivers.append(rivers[count])
        else:
            next_river_equal = False
        count += 1
    #checks if next rivers have same number of stations, if so then add to list
    
    return first_N_rivers
                





