# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 17:52:34 2019

@author: User
"""

import numpy as np
import matplotlib

def polyfit(dates, levels, p):
    # convert dates to function arguments
    x = matplotlib.dates.date2num(dates)
    
    shift = x[0]
    
    #use polyfit to create coefficients of degree p
    p_coeff = np.polyfit(x - shift, levels, p)
    
    #convert coefficients to a polynomial that can be plotted
    poly = np.poly1d(p_coeff)
    
    polynomial = [poly, shift]
    return polynomial
    