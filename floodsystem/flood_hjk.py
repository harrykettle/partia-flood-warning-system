# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 11:23:34 2019

@author: Harry Kettle
"""

#2B
from floodsystem.stationdata import update_water_levels, build_station_list



  
def stations_level_over_threshold(stations, tol):


    risk_stations = []
    for station in stations:
       
            if station.relative_water_level() is not None and station.relative_water_level() > tol:
               
                new_tuple = (station, station.relative_water_level())
                risk_stations.append(new_tuple)
    risk_stations.sort(key=lambda x: float(x[1]), reverse=True)
    return risk_stations[:10]
        
#===========================================================

#2C
def stations_highest_rel_level(stations, N):
    #stations = build_station_list()
    first_N_rivers = []
    first_N_rivers = stations_level_over_threshold(stations, 0)[:N]
    #print(type(stations_level_over_threshold(stations, 0)))
    return first_N_rivers[:N]
    
    #first_N_rivers = stations_level_over_threshold [:N]
  
    #next_river_equal = True
    #count = N
    #while(next_river_equal):
     #   if (risk_stations[count][1] == first_N_rivers[count-1][1]):
      #      first_N_rivers.append(rivers[count])
       # else:
        #    next_river_equal = False
        #count += 1
    #checks if next rivers have same number of stations, if so then add to list
    






#========================================================
    


stations = build_station_list()
    #build empty list of stations
    
update_water_levels(stations)
    
over_tolerance_stations = stations_level_over_threshold(stations, 0.8)
    #print the name and current level for each station
for entry in over_tolerance_stations:
    print("{}, : {}".format(entry[0].name, entry[1]))

        
        
    