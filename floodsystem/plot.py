# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 20:35:24 2019

@author: User
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from floodsystem.analysis import polyfit

def plot_water_levels(station, dates, levels):
    
    #creating lists to hold low and high level range information
    low = [station.typical_range[0]]*len(dates)
    high = [station.typical_range[1]]*len(dates)
    
    #plotting the lines
    plt.plot(dates, levels, label = 'Latest Levels')
    plt.plot(dates, low, label = 'Typical Low Level')
    plt.plot(dates, high, label = 'Typical High Level')
    
    #labelling the axes, adding titles and orienting labels
    plt.xlabel('Date')
    plt.ylabel('Water Level / m')
    plt.xticks(rotation=45)
    plt.title('Station name: {}'.format(station.name))
    
    #display graph
    plt.tight_layout()
    plt.show()
    
def plot_water_level_with_fit(station, dates, levels, p):
    
    #original data
    plt.plot(dates, levels, '.')
    
    #plotting the polynomial
    poly, shift = polyfit(dates, levels, p)
    
    #getting values for the x axis
    x = matplotlib.dates.date2num(dates)
    
    #creating lists to hold low and high level range information
    low = [station.typical_range[0]]*len(dates)
    high = [station.typical_range[1]]*len(dates)
    
    #plotting the lines
    plt.plot(dates, low, label = 'Typical Low Level')
    plt.plot(dates, high, label = 'Typical High Level')

    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1 - shift))
    
    #labelling the axes, adding titles and orienting labels
    plt.xlabel('Date')
    plt.ylabel('Water Level / m')
    plt.xticks(rotation=45)
    plt.title('Station name: {}'.format(station.name))
    
    #display graph
    plt.tight_layout()
    plt.show()