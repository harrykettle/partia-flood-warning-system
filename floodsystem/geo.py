# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa

# Task 1B

import numpy as np

def radian(x):
    """Converting degrees into radians"""
    y = x/180*np.pi
    return y

def haversine(a, b):
    """Haversine formula to calculate the distance between two sets of coordinates in km"""
    alat = radian(a[0])
    along = radian(a[1])
    blat = radian(b[0])
    blong = radian(b[1])
    d = 12742*float(np.arcsin(float(np.sqrt(((float(np.sin((blat-alat)/2)))**2)+(float(np.cos(alat))*float(np.cos(blat))*((float(np.sin((blong-along)/2)))**2))))))
    return (d)

def stations_by_distance(stations, p):
    """Calculating the distances between monitoring stations and a fixed set of coordinates p"""
    distances = []
    for s in stations:
        dist = haversine(s.coord, p)
        item = (s.name, dist)
        distances.append(item)
    
    sorted_distances = sorted_by_key(distances, 1)
    return sorted_distances



#Task 1D

def rivers_with_stations(stations):
    """Creating a list of all the rivers which have monitoring stations"""
    rivers = []
    for s in stations:
        if s.river in rivers:
            pass
        else:
            rivers.append(s.river)
        
    return rivers

def stations_by_river(stations):
    """Creating a dictionary where the keys are rivers and the values are lists of all the monitoring stations on each river"""
    rivers_dict = {}
    #for each object check if the river is already in the dictionary, if not then open a list
    for s in stations:
        if s.river in rivers_dict:
            pass
        else:
            items = []
            a = s.river
            for t in stations:
                if a == t.river:
                    items.append(t.name)
                else:
                    pass
        rivers_dict.update({a: items})
    return rivers_dict




#1C
#from .utils import sorted_by_key  # noqa

import numpy as np

def radian(x):
    """Converting degrees into radians"""
    y = x/180*np.pi
    return y


#makes list of stations within radius
def stations_within_radius(stations, centre, r):
    #create empty list to hold stations within r
    stations_in_radius = []
    
    for i in stations:
        dist = haversin (centre, i.coord)
        #calculate distance from centre
        if (dist < r):
            stations_in_radius.append(i)
        #if within radius add to list
    return stations_within_radius







#1E
def rivers_by_station_number(stations, N):
    rivers = []
    #creates list of empty rivers
    for j in stations:
        if(len(rivers) == 0):
            new_river = (j.river, '1')
            rivers.append(new_river)
            #if list of rivers empty add new river+station
        else:
            river_found = False
            for l in range(len(rivers)):
                if(j.river == rivers[l][0]):
                    new_value = int(rivers[l][1])+1
                    tuple_to_change = list(rivers[l])
                    tuple_to_change[1] = str(new_value)
                    rivers[l] = tuple(tuple_to_change)
                    river_found = True
                    break;
                #if station is assigned to a river, update tuple to add station number
                #if river found break loop
            if(river_found == False):
                new_river = (j.river, '1')
                rivers.append(new_river)
                #adds river to list if not on it
                
    rivers.sort(key = lambda x: int(x[1]), reverse = True)
    #sort rivers into descending order of stations
    
    first_N_rivers = rivers[:N]
    #finds first N rivers in sorted list
    
    next_river_equal = True
    count = N
    while(next_river_equal):
        if (rivers[count][1] == first_N_rivers[count-1][1]):
            first_N_rivers.append(rivers[count])
        else:
            next_river_equal = False
        count += 1
    #checks if next rivers have same number of stations, if so then add to list
    
    return first_N_rivers
                







