# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 14:29:49 2019

@author: User
"""

from floodsystem.stationdata import build_station_list, update_water_levels
#from stationdata.flood_hjk import stations_highest_rel_level, stations_level_over_threshold

stations = build_station_list()
update_water_levels(stations)

a = []
for s in stations:
        a.append([s.name, s.town, s.relative_water_level()])
    
Yellow = 0.75
Orange = 1
Red = 1.25

Low_list = []
Moderate_list = []
High_list = []
Severe_list = []

for item in a:
    if item[2] == None:
        pass
    elif item[2] < Yellow:
        Low_list.append(item)
    elif item[2] >= Yellow and item[2] < Orange:
        Moderate_list.append(item)
    elif item[2] >= Orange and item[2] < Red:
        High_list.append(item)
    else:
        Severe_list.append(item)
        
Severe = []
High = []
Moderate = []

for x in Severe_list:
    Severe.append(x[0])
for x in High_list:
    High.append(x[0])
for x in Moderate_list:
    Moderate.append(x[0])
    
Severe.sort()
High.sort()
Moderate.sort()
    
print("The following stations are at severe risk of flooding: {}".format(Severe))
print("The following stations are at high risk of flooding: {}".format(High))        
print("The following stations are at moderate risk of flooding: {}".format(Moderate))