# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 17:36:47 2019

@author: User
"""

from floodsystem.stationdata import build_station_list
import floodsystem.geo as geo
import numpy as np

def test_radian():
    x = geo.radian(4)
    assert x == 4/180*np.pi
    
def test_haversine():
    x = geo.haversine((1.0, 1.0), (1.0, 1.0))
    y = geo.haversine((1.0, 1.5), (2.0, 2.5))
    assert x == 0
    assert round(y, 5) == 7121.62037
 
Stations = build_station_list()    

def test_distances():
    x = geo.stations_by_distance(Stations, (52.2053, 0.1218))
    
    for a in x[:5]:
        assert a <= 7.1
    
    for a in x[-5:]:
        assert a >= 449

def test_rivers_with_station():
    river_list = geo.rivers_with_station(Stations)
    assert len(river_list) == 857
    
def test_stations_by_river():
    river_station = geo.stations_by_river(Stations)

    assert len(river_station['River Aire']) == 23
    assert len(river_station['River Cam']) == 7
    assert len(river_station['Thames']) == 55