# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 07:32:58 2019

@author: User
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood_hjk import stations_highest_rel_level


stations = build_station_list()
update_water_levels(stations)

a = stations_highest_rel_level(stations, 5)

        
for station in a:
    dt = 2
    dates, levels = fetch_measure_levels(station[0].measure_id, dt = datetime.timedelta(days=dt))
    
    if len(dates) == 0 or len(levels) == 0:
        continue
    plot_water_level_with_fit(station[0], dates, levels, 4)