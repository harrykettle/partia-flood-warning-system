# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 20:20:56 2019

@author: User
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river

#build a list of stations
Stations = build_station_list()

"""Requirements for Task1D"""

print (len(rivers_with_stations(Stations)))
list1 = rivers_with_stations(Stations)
list1.sort()
print (list1[0:10])


dict1 = stations_by_river(Stations)
list2 = dict1["River Aire"]
list2.sort()
list3 = dict1["River Cam"]
list3.sort()
list4 = dict1["River Thames"]
list4.sort()
print ("\nThe monitoring stations on the River Aire are {}\n".format(list2))
print ("The monitoring stations on the River Cam are {}\n".format(list3))
print ("The monitoring stations on the River Thames are {}\n".format(list4))