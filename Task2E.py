# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 20:47:19 2019

@author: User
"""

from floodsystem.stationdata import build_station_list, update_water_levels
#import operator
import datetime
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
#import matplotlib
#import matplotlib.pyplot as plt

from floodsystem.flood_hjk import stations_highest_rel_level

stations = build_station_list()
update_water_levels(stations)

#getting stations with 5 highest water levels
#find list of current water levels
#a = []
#for s in stations:
    #a.append((s.name, s.latest_level))

#exclude items with no water_level_data
#b=[]
#for t in a:
    #if t[1] == None:
       # pass
    #else:
        #b.append(t)

#b.sort(key = operator.itemgetter(1), reverse = True)

#c = b[0:10]

#d = []
#for x in c:
    #for y in stations:
        #if x[0] == y.name:
            #d.append(y)
        #else:
            #pass  

#print(d)

a = stations_highest_rel_level(stations, 5)

           
#finding station
for station in a:
    dates_list = []
    levels_list = []
    dt = 10
    dates, levels = fetch_measure_levels(station[0].measure_id, dt= datetime.timedelta(days=dt))
    
    for date, level in zip(dates, levels):
        dates_list.append(date)
        levels_list.append(level)
    
    plot_water_levels(station[0], dates, levels)   
    
    #plt.plot(dates, levels)
    #plt.show()




