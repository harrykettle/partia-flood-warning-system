# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 15:21:40 2019

@author: User
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood_hjk import stations_level_over_threshold


stations = build_station_list()
    #build empty list of stations
    
update_water_levels(stations)
    
over_tolerance_stations = stations_level_over_threshold(stations, 0.8)
    #print the name and current level for each station
for entry in over_tolerance_stations:
    print("{}, : {}".format(entry[0].name, entry[1]))