# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 15:22:52 2019

@author: Harry Kettle
"""
from floodsystem.station import MonitoringStation
from floodsystem.flood_hjk import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels, build_station_list


def test_2B():
    x = build_station_list()
    update_water_levels(x)
    y = stations_level_over_threshold(x, 0.5)
    for i in range(len(y)-1):
        assert y[i][0].relative_water_level() >= y[i+1][0].relative_water_level()
        assert y[i][0].relative_water_level() >= 0.5
        #checks water levels are over tolerance and in order
        
test_2B()
   

