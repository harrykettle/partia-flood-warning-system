# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 09:04:15 2019

@author: Harry Kettle
"""

#2C
from floodsystem.flood_hjk import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    at_risk = stations_highest_rel_level(stations, 10)

    #print the name and current level for each station
    for entry in at_risk:
        names[station[0].name] = station[0].relative_water_levels()
    print(names)
    
if __name__ == "__main__":
    run()


#def run():
   # stations = build_station_list()
    #build empty list of stations
    
   # N = 10
    #prints first N in list
    
   # first_N_rivers = stations_highest_rel_level(stations, N)
    #find first N rivers with highest relative water levels
    
   # print(first_N_rivers)
    
    